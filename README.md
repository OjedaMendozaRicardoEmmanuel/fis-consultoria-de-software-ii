# Consultoría de software II
## 1. Platzi
### Logotipo: ![Una imagen](imagenes/una-imagen.jpg)
### Nombre: Platzi
### Sitio web: [platzi.com](https://platzi.com)
### Ubicación: [Av. Paseo de la Reforma 373-Piso 20, Cuauhtémoc, 06500 Ciudad de México, CDMX](https://goo.gl/maps/qa4r5TdFoP1QykMs7)
### Acerca de
- Platzi está formando el siguiente millón de profesionales en tecnología, transformando en economías digitales a países en vías de desarrollo. Mediante una plataforma y una metodología propias, que nos permiten hacer educación online efectiva enfocada en las necesidades reales de nuestra comunidad latinoamericana.
### Servicios 
- Ingeniería y desarrollo
- Diseño y UX
- Inglés
- Marketing
- Emprendimiento y negocios
- Producción audiovisual ¡y más!
### Presencia
- [Twitter](https://twitter.com/platzi)
- [Youtube](https://www.youtube.com/channel/UC55-mxUj5Nj3niXFReG44OQ)
- [Facebook](https://www.facebook.com/platzi)
- [Instagram](https://www.instagram.com/platzi/)
### Ofertas laborales [Bolsa De Trabajo](https://platzi.com/blog/bolsa-trabajo-sistema-referidos/)
### Blog de Ingeniería [Blog](https://platzi.com/blog/)
### Tecnologías
- React.js.
- Python.
- Django.
- iOS y Android.

## 2. AMK Technologies
### Logotipo: ![Una imagen](imagenes/una-imagen.jpg)
### Nombre: AMK Technologies
### Sitio web: [amk-technologies.com](http://amk-technologies.com/)
### Ubicación: [C. Tlacoquemecatl 21, Del Valle, Benito Juárez, 03200 Ciudad de México, CDMX](https://goo.gl/maps/z7V2DBRKFUAKpcs89)
### Acerca de
- AMK Technologies  es una empresa boutique de tecnología ágil, 100% mexicana que forma parte de Grupo Allié. Reconocida como una de las mejores Tech consulting en México y como una empresa Best Place to Code en 2021.
### Servicios 
- Desarrollo de Proyectos Llave en Mano
- Aplicaciones Móviles Empresariales 
- Consultoría Especializada 
- Staffing
### Presencia
- [Twitter](https://twitter.com/AMKTechnologies)
- [Linkedin](https://mx.linkedin.com/company/amk-technologies)
- [Facebook](https://www.facebook.com/AMKTechnologiesMx/)
- [Instagram](https://www.instagram.com/amktechnologies/)
### Ofertas laborales [Bolsa De Trabajo](mailto:contacto@amk-technologies.com)
### Blog de Ingeniería [Blog](http://www.amktechsolutions.com/blog/)
### Tecnologías
- Java.
- .NET.
- API.

## 3. Aviada
### Logotipo: ![Una imagen](imagenes/una-imagen.jpg)
### Nombre: Aviada
### Sitio web: [aviada.mx](https://aviada.mx/)
### Ubicación: [Bulevar Luis D. Colosio 671-12vo Piso, Santa Fe, 83249 Hermosillo, Son.](https://g.page/aviada?share)
### Acerca de
- Somos una empresa 100% mexicana fundada en 2017, hecha por empleados para empleados, comprometidos con hacer siempre lo correcto y lo justo. Nuestros clientes (empresas responsables y bien establecidas) se encuentran principalmente en California y sus actividades son muy variadas: Entretenimiento, Publicidad, Compras Online, etc.
### Servicios 
- Recruitment
- Legal compliance
- Payroll processing
- HR
- Procurement
- Equipment Leasing
- Recruitment
### Presencia
- [Twitter](https://twitter.com/AMKTechnologies)
- [Linkedin](https://mx.linkedin.com/company/amk-technologies)
- [Facebook](https://www.facebook.com/AMKTechnologiesMx/)
- [Instagram](https://www.instagram.com/amktechnologies/)
### Ofertas laborales [Bolsa De Trabajo](https://careers.aviada.mx/)
### Blog de Ingeniería [Blog](https://aviada.mx/category/blog/)
### Tecnologías
- Android
- PHP
- Python
- DevOps
- E-Commerce
- Javascript
- NodeJS
- Swift
